#!/bin/bash

set -u
set -e

export HEADLESS=true

ENV_VARS="BIPIO_HOSTNAME BIPIO_API_HOST BIPIO_PROTOCOL BIPIO_ADMIN_PASSWORD
         BIPIO_AES_KEY LDAP_SERVER ADMIN_URL LDAP_BUS_URL
         MONGODB_PORT_27017_TCP_ADDR JWT_KEY AUTH_HOST TWITTER_KEY
         TWITTER_SECRET FACEBOOK_ID FACEBOOK_SECRET DROPBOX_KEY DROPBOX_SECRET
         INSTAGRAM_ID INSTAGRAM_SECRET TUMBLR_KEY TUMBLR_SECRET VIMEO_ID
         VIMEO_SECRET SMTP_HOST SMTP_USER SMTP_PASS BITLY_ID BITLY_SECRET
         GITHUB_ID GITHUB_SECRET MAILCHIMP_ID MAILCHIMP_SECRET TRELLO_KEY
         TRELLO_SECRET"

echo "Updating config file with environment variables."
for i in ${ENV_VARS}; do
  val=$(echo ${!i} | sed -e 's/\//\\\//g' | sed -e 's/\@/\\@/g')
  perl -pi -e "s/${i}/${val}/g" /var/local/bipio/config/config.json-dist
done

/etc/init.d/rabbitmq-server start

echo "Updating bipio-server config."
cd /var/local/bipio
cp ./config/config.json-dist ./config/default.json
mkdir -p /data/server_logs

echo "Running 'make install'"
make install > /data/server_logs/server.log 2>&1

echo "Running bipio-server."
supervisor ./src/server.js >> /data/server_logs/server.log 2>&1 &
/bin/bash -l