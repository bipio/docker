#!/usr/bin/python

import random


def rand(num=16):
    chars = []
    r = random.SystemRandom()

    for i in range(num):
       chars.append(r.choice('ABCDEF0123456789'))

    return ''.join(chars)

print 'BIPIO_AES_KEY: %s' % rand(16)
print 'JWT_KEY: %s' % rand(48)
print 'BIPIO_ADMIN_PASSWORD: %s' % rand(16)