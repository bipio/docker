FROM node:6.11.2
MAINTAINER WoT.io Devs <dev@wot.io>

RUN mkdir -p /data/db
RUN mkdir -p /data/server_logs

VOLUME ["/data"]

# Install RabbitMQ

ADD rabbitmq-signing-key-public.asc /tmp/rabbitmq-signing-key-public.asc
RUN apt-key add /tmp/rabbitmq-signing-key-public.asc \
	&& echo "deb http://www.rabbitmq.com/debian/ testing main" > /etc/apt/sources.list.d/rabbitmq.list \
	&& apt-get -qq update > /dev/null \
	&& apt-get -q -y install sudo \
	&& apt-get -qq -y --force-yes install rabbitmq-server > /dev/null \
	&& /usr/sbin/rabbitmq-plugins enable rabbitmq_management \
	&& echo "[{rabbit, [{loopback_users, []}]}]." > /etc/rabbitmq/rabbitmq.config

EXPOSE 5672 15672 4369


# Install MongoDB: Import MongoDB public GPG key AND create a MongoDB list file
RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10 \
	&& echo "deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list \
	&& apt-get update > /dev/null \
	&& apt-get -q -y install \
		nodejs\
		npm \
		git \
		mongodb-org 

CMD ["mongod", "--dbpath", "."]

EXPOSE 27017


# Start with a known base
WORKDIR /root


# Install supervisor
RUN npm install -g supervisor


# Install BipIO
WORKDIR /usr/local/lib/node_modules

RUN git clone https://gitlab.com/bipio/api-server.git bipio

WORKDIR /usr/local/lib/node_modules/bipio
RUN npm install

# Add config
RUN mv /etc/localtime /etc/localtime.bak \
	&& ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime

EXPOSE 5000

ADD ./bootstrap.sh /root/bootstrap.sh

RUN chmod 755 /root/*.sh

CMD /root/bootstrap.sh

